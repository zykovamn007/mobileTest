package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BasePage {

    protected WebDriver driver;
    protected String path;
    public static String basePath;



    public WebElement getElementById(String id){
        return driver.findElement(By.id(id));
    }

    public WebElement getElementXpath(String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public WebElement getElementCss(String css){
        return driver.findElement(By.cssSelector(css));
    }

}


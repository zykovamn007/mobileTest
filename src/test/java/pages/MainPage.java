package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePage{

    public MainPage(WebDriver driver){
        this.driver = driver;
        this.path = "https://www.drom.ru/";
    }

    public MainPage open(){
        driver.get(path);
        return this;
    }

    public MainPage clickBrand(String brand)  {
        getElementXpath(".//a[contains(text(),'" + brand +
                "')][@data-ftid='component_cars-list-item_hidden-link']").click();
        return this;
    }

    public String getTitleBrande()  {
        return getElementXpath("//h1[contains(text(),'Продажа ')]").getText();
    }



}

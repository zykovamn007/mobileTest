import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.MainPage;

import java.io.File;

class MobileTest {
    private DesiredCapabilities caps;
    private AppiumDriver driver;

    @BeforeEach
    public void BeforeTest(){
        caps = new DesiredCapabilities();

        caps.setCapability("chromedriverExecutable",
                System.getProperty("user.dir")
                        + File.separator + "chromedriver_win32" +  File.separator + "chromedriver.exe");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        caps.setCapability(MobileCapabilityType.BROWSER_NAME,"chrome");
        driver = new AppiumDriver<>(caps);
    }

    @AfterEach
    public void AfterTest(){
        driver.quit();
    }


    @Test
    void ChromeTesting() {
        MainPage mainPage = new MainPage(driver);
        Assertions.assertTrue(mainPage.open().clickBrand("Audi").getTitleBrande().contains("Audi"));

    }


}